// c++
#include <iostream>
#include <fstream>
#include <filesystem>

// ea
#include <include/console.hpp>

namespace message
{
	int32_t example (void) noexcept
	{
		//
		ea::console::color::set (240);
		std::cout << "BoxCompiler.exe output_file_path";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << "\npack all files in current directory\n\n";

		//
		ea::console::color::set (240);
		std::cout << "BoxCompiler.exe output_file_path -dir directory_name";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << "\npack all files in specified directory\n\n";

		//
		ea::console::color::set (240);
		std::cout << "BoxCompiler.exe output_file_path -sub";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << "\npack all files in current directory with subdirectories\n\n";

		//
		ea::console::color::set (240);
		std::cout << "BoxCompiler.exe output_file_path -sub -dir directory_name";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << "\npack all files in specified directory with subdirectories\n\n";

		return EXIT_SUCCESS;
	} // int32_t example (void) noexcept

	template <typename Path>
	int32_t cant_create (Path const& _path) noexcept
	{
		std::cout << "file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "can't created";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		return EXIT_FAILURE;
	} // int32_t cant_create (Path const& _path) noexcept

	template <typename Path>
	int32_t no_exist (Path const& _path) noexcept
	{
		std::cout << "file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "not found";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		return EXIT_FAILURE;
	} // int32_t no_exist (Path const& _path) noexcept

	template <typename Path>
	int32_t cant_read (Path const& _path) noexcept
	{
		std::cout << "file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "can't be read";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		return EXIT_FAILURE;
	} // int32_t cant_read (Path const& _path) noexcept

	void ok (uint32_t const _offset, std::string const& _path) noexcept
	{
		ea::console::color::set (240);
		std::cout << "0x" << std::setfill ('0') << std::setw (std::numeric_limits <uint32_t>::digits10) << std::hex << _offset;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << ' ' << _path << ' ';

		ea::console::color::set (240);
		std::cout << "ok";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << '\n';
	} // void ok (uint32_t const _offset, std::string const& _path) noexcept
} // namespace message

class pack
{
	class packer {
	public:
		template <typename Iterator, typename Path, typename MapPath>
		int32_t pack (Iterator _iterator, Path _path, MapPath _map_path) noexcept
		{
			if (!archive) {
				message::cant_create (_path);
				return EXIT_FAILURE;
			}
			if (!map) {
				message::no_exist (_map_path);
				return EXIT_FAILURE;
			}

			uint32_t files{ 0 };
			map.write (reinterpret_cast <const char*> (&files), sizeof (files));
			archive.write (reinterpret_cast <const char*> (&files), sizeof (files));

			for (auto const& entry : _iterator) {
				if (!entry.is_regular_file ()) { continue; }
				const auto path{ entry.path ().generic_string () };

				const uint32_t offset{ static_cast <uint32_t> (archive.tellp ()) };
				const auto file_raw_size{ this->process_raw_file (path) };
				this->process_map (offset, path);

				message::ok (offset, path);
				++files;
			}

			archive.seekp (0, std::ios::beg);
			archive.write (reinterpret_cast <const char*> (&files), sizeof (files));

			map.seekp (0, std::ios::beg);
			map.write (reinterpret_cast <const char*> (&files), sizeof (files));

			return EXIT_SUCCESS;
		}

		template <typename Path, typename MapPath>
		packer (Path const* _output_path, MapPath _map_path) noexcept :
			archive{ _output_path, std::ofstream::binary },
			map{ _map_path, std::ofstream::binary }
		{}
	private:
		std::string read_raw (std::string const& _path) noexcept
		{
			std::string file_raw;

			std::ifstream file{ _path };
			if (!file) {
				message::cant_read (_path);
				return file_raw;
			}

			file.seekg (0, std::ios::end);
			file_raw.reserve (file.tellg ());
			file.seekg (0, std::ios::beg);

			file_raw.assign ((std::istreambuf_iterator <char> (file)), std::istreambuf_iterator<char> ());

			return file_raw;
		}

		uint32_t process_raw_file (std::string const& _path) noexcept
		{
			// archive file
			auto const file_raw{ this->read_raw (_path) };
			auto const file_raw_size{ static_cast <uint32_t> (std::size (file_raw)) };

			// file size
			archive.write (reinterpret_cast <const char*> (&file_raw_size), sizeof (file_raw_size));

			// file
			archive.write (std::data (file_raw), file_raw_size);
			return file_raw_size;
		}

		void process_map (uint32_t const _offset, std::string const& _path)
		{
			// map file
			map.write (reinterpret_cast <const char*> (&_offset), sizeof (_offset));

			const auto path_length{ static_cast <uint8_t> (std::size (_path)) };

			// path length
			map.write (reinterpret_cast <const char*> (&path_length), sizeof (path_length));

			// path
			map.write (std::data (_path), path_length);
		}

		std::ofstream archive;
		std::ofstream map;
	};

	template <typename Path, typename Iterator>
	static int32_t _pack (Path const* _output_path, Iterator _iterator) noexcept
	{
		const auto map{ std::string{ _output_path }.append (".map") };
		packer packer{ _output_path, map };
		return packer.pack (_iterator, _output_path, map);
	} // static int32_t _pack (int32_t const argc, char const* argv[]) noexcept
public:
	static int32_t all (int32_t const argc, char const* argv[]) noexcept
	{
		return pack::_pack (argv[1], std::filesystem::directory_iterator (std::filesystem::current_path ()));
	} // static int32_t all (int32_t const argc, char const* argv[]) noexcept

	static int32_t all_from_folder (int32_t const argc, char const* argv[]) noexcept
	{
		return pack::_pack (argv[1], std::filesystem::directory_iterator (std::filesystem::path (argv[3])));
	} // static int32_t all_from_folder (int32_t const argc, char const* argv[]) noexcept

	static int32_t recursive (int32_t const argc, char const* argv[]) noexcept
	{
		return pack::_pack (argv[1], std::filesystem::recursive_directory_iterator (std::filesystem::current_path ()));
	} // static int32_t recursive (int32_t const argc, char const* argv[]) noexcept

	static int32_t recursive_from_folder (int32_t const argc, char const* argv[]) noexcept
	{
		return pack::_pack (argv[1], std::filesystem::recursive_directory_iterator (std::filesystem::path (argv[4])));
	} // static int32_t recursive (int32_t const argc, char const* argv[]) noexcept
}; // class pack

int32_t main (int32_t const argc, char const* argv[]) noexcept
{
	// pack all files in current directory
	if (2 == argc) { return pack::all (argc, argv); }
	// pack all files in specified directory
	else if (4 == argc && 0 == strcmp ("-dir", argv[2])) { return pack::all_from_folder (argc, argv); }
	// pack all files in current directory with subdirectories
	else if (3 == argc && 0 == strcmp ("-sub", argv[2])) { return pack::recursive (argc, argv); }
	// pack all files in specified directory with subdirectories
	else if (5 == argc && 0 == strcmp ("-sub", argv[2]) && 0 == strcmp ("-dir", argv[3])) { return pack::recursive_from_folder (argc, argv); }

	return message::example ();
} // int32_t main (int32_t const argc, char const* argv[]) noexcept